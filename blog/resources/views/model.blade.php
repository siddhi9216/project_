@extends('layout')
@section('content')
    <div class="container-fluid">
        <div class="card col-lg-10 offset-1 mt-5">
            <div class="card-header header-elements-inline" style="background-color: white">
                <legend class="text-uppercase font-size-sm font-weight-bold">Employees Data</legend>
            </div>
            <div class="card-body">
                <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper no-footer mt-2">
                    <div class="datatable-header">
                        <div id="DataTables_Table_1_filter" class="dataTables_filter"><label><span>Filter:</span> <input
                                    type="search" class="" placeholder="Type to filter..."
                                    aria-controls="DataTables_Table_1"></label></div>
                        <div class="dataTables_length" id="DataTables_Table_1_length"><label><span>Show:</span> <select
                                    name="DataTables_Table_1_length" aria-controls="DataTables_Table_1"
                                    class="select2-hidden-accessible" data-select2-id="4" tabindex="-1"
                                    aria-hidden="true">
                                    <option value="10" data-select2-id="6">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr"
                                               data-select2-id="5" style="width: auto;"><span class="selection"><span
                                            class="select2-selection select2-selection--single" role="combobox"
                                            aria-haspopup="true" aria-expanded="false" tabindex="0"
                                            aria-disabled="false"
                                            aria-labelledby="select2-DataTables_Table_1_length-k3-container"><span
                                                class="select2-selection__rendered"
                                                id="select2-DataTables_Table_1_length-k3-container" role="textbox"
                                                aria-readonly="true" title="10">10</span><span
                                                class="select2-selection__arrow" role="presentation"><b
                                                    role="presentation"></b></span></span></span><span
                                        class="dropdown-wrapper" aria-hidden="true"></span></span></label></div>
                    </div>
                    <div class="datatable-scroll">
                        <table
                            class="table  table-xs font-size-sm table-bordered table-hover  dataTables_pagination datatable-highlight dataTable datatable-save-state table-striped"
                            role="grid" id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info">
                            <thead>
                            <tr>

                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="First Name: activate to sort column descending">First Name
                                </th>
                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="BIrthDate: activate to sort column descending">lastname
                                </th>
                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="GEnder: activate to sort column descending">email
                                </th>
                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="Address: activate to sort column descending">dob
                                </th>
                                <th class="sorting_asc" aria-controls="DataTables_Table_1" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="Mobile No activate to sort column descending">phone
                                </th>
                                <th class="sorting_asc" aria-controls="DataTables_Table_1" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="Desciption No activate to sort column descending">address
                                </th>

                                <th class="sorting_asc" aria-controls="DataTables_Table_1" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="Delete No activate to sort column descending">Delete
                                </th>
                                <th class="sorting_asc" aria-controls="DataTables_Table_1" rowspan="1"
                                    colspan="1" aria-sort="ascending"
                                    aria-label="Edit No activate to sort column descending">Edit
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $data)
                                <tr role="row" class="odd">

                                    <td class="sorting_1">{{$data->firstname}}</td>
                                    <td>{{$data->lastname}}</td>
                                    <td>{{$data->email}}</td>
                                    <td>{{$data->dob}}</td>
                                    <td>{{$data->phone}}</td>
                                    <td>{{$data->address}}</td>
                                    <td>
                                        <form method="post" class="delete_form" action="/employee/{{$data->id}}">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE"/>
                                            <button type="submit" class="btn btn-danger" data-id="{{$data->id}}"
                                                    id="deleteform">Delete
                                            </button>
                                        </form>
                                    </td>
                                    <td><a type="button" class="btn btn-success"
                                           href="#">Edit</a></td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                        <div class="datatable-footer">

                            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a
                                    class="paginate_button previous" aria-controls="DataTables_Table_0" data-dt-idx="0"
                                    tabindex="0" id="DataTables_Table_0_previous">←</a><span><a
                                        class=" paginate_button current"
                                        aria-controls="DataTables_Table_0"
                                        data-dt-idx="1"
                                        tabindex="0">1</a><a
                                        class="paginate_button" aria-controls="DataTables_Table_0" data-dt-idx="2"
                                        tabindex="0">2</a></span><a class="paginate_button next disabled"
                                                                    aria-controls="DataTables_Table_0" data-dt-idx="3"
                                                                    tabindex="0" id="DataTables_Table_0_next">→</a>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#modal_form_horizontal">
                                Add<i class="icon-play3 ml-2"></i></button>
                        </div>
                    </div>
                </div>
        </div>

        <script>

            $(document).ready(function () {
                $('#deleteform').click(function (e) {
                    var id = $(this).val();
                    var url = "{{url('employee')}}";
                    var dturl = url + "/" + id;
                    $.ajax({
                        url: dturl,
                        type: "DELETE",
                        cache: false,
                        data: {
                            _token: '{{csrf_token()}}'
                        },
                        success: function (dataResult) {
                            var dataResult = JSON.parse(dataResult);
                            if (dataResult.statusCode == 200) {
                                $ele.fadeOut().remove();
                            }
                        }
                    });
                });
            });


        </script>
        <script type="text/javascript" src="{{asset('js/validation/jquery.validate.min.js')}}"></script>
        <script>
            $(document).ready(function () {
                $('#form').validate({
                    rules: {
                        firstname: {
                            required: true,
                            maxlength: 10,

                        },
                        lastname: {
                            required: true,
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        dob: {
                            required: true,
                            date: true
                        },
                        phone: {
                            required: true,
                            minlength: 10,
                            maxlength: 10,
                            digits: true
                        },
                        address: {
                            required: true,
                            maxlength: 50

                        }

                    }
                });
            });
        </script>
@endsection
