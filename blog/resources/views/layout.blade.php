<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script type="text/javascript" src="{{asset('js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/core/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/core/bootstrap.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/core/blockui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/themes/bootbox.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/themes/components_models.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/themes/datables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/themes/datatables_basic.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/themes/select2.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>

    <link href="css/styles.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="css/components.min.css" rel="stylesheet" type="text/css">
    <link href="css/colors.min.css" rel="stylesheet" type="text/css">

    <title>Document</title>

</head>
<body>

<div class="container-fluid">
    <div id="modal_form_horizontal" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Horizontal form</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <form action="{{route('employee.store')}}" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">First name</label>
                            <div class="col-sm-9">
                                <input type="text" name="firstname" placeholder="Eugene" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Last name</label>
                            <div class="col-sm-9">
                                <input type="text" name="lastname" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Email</label>
                            <div class="col-sm-9">
                                <input type="text" name="email" placeholder="eugene@kopyov.com" class="form-control">
                                <span class="form-text text-muted">name@domain.com</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Dob #</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" name="dob">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Phone #</label>
                            <div class="col-sm-9">
                                <input type="tel" name="phone" class="form-control">

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Address line 1</label>
                            <div class="col-sm-9">
 <textarea rows="5" cols="5" class="form-control" placeholder="Enter your address here" name="address">
                        </textarea></div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn bg-primary">Submit form</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @yield('content')


</div>
</body>
</html>
